
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:namaz_app/utils/application_constants.dart';

class Settings extends StatefulWidget{


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    _SettingsState();
  }
}
class _SettingsState extends State<Settings>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
      return(MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: ApplicationConstants.settingsAppbar,
            leading: IconButton(onPressed: (){}, icon: Icon(Icons.arrow_back,color: Colors.white,)),
            title: Text(
              'Settings',
            ),
          ),
        )
      )
      );
  }
}
