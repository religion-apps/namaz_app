import 'package:flutter/material.dart';
import 'package:namaz_app/utils/application_constants.dart';

class QuranReading extends StatefulWidget {

  @override
  _QuranReadingState createState() => _QuranReadingState();
}

class _QuranReadingState extends State<QuranReading> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  child: Text(
                    'Drawer Header',
                    style: TextStyle(
                        fontFamily: 'Avenir'

                    ),
                  )
              ),
              ListTile(
                title: Text('Namaz'),
                onTap: (){

                },
              )

            ],
          ),
        ),
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            iconTheme: IconThemeData(color: ApplicationConstants.drawerIconColor),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                /*  Container(
                  child: Text('Namaz App',
                  style: TextStyle(
                    color: Colors.black
                  ),),

                ),*/
                Container(
                  alignment: Alignment.centerRight,
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
                          fit: BoxFit.fill
                      )
                  ),
                )
              ],
            )

        ),
        body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start  ,
                children: <Widget>[
                  Text('Al Quran',
                    style: TextStyle(
                      color: ApplicationConstants.grayTextColor,
                      fontSize: 20,
                      fontFamily: 'Avenir'
                    )
                  ),
                   SizedBox(height: 3,),
                   Text(
                      'Reading Surah',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Avenir',
                        fontSize: 30,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  SizedBox(height: 8,),
                  Container(

                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: ApplicationConstants.arrowBackground
                    ),
                    /*child: Expanded(
                      child: Row(
                        children: <Widget>[
                          TextField(
                            style: TextStyle(

                            ),
                            decoration: InputDecoration(
                              hintText: "Search Surah",
                              hintStyle: TextStyle(
                                color: ApplicationConstants.grayTextColor
                              )
                            ),
                          )
                        ],
                      ),
                    ),*/
                  ),

                ],
              ),
            )
        ),

      ),
    );
  }
}
