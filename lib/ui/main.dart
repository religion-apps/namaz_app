
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:namaz_app/ui/quran.dart';
import 'package:namaz_app/ui/settings.dart';
import 'package:namaz_app/utils/application_constants.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        backgroundColor: ApplicationConstants.backgroundMain,
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  child: Text(
                    'Drawer Header',
                    style: TextStyle(
                        fontFamily: 'Avenir'

                    ),
                  )
              ),
              ListTile(
                title: Text('Namaz'),
                onTap: (){

                },
              )

            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          iconTheme: IconThemeData(color: ApplicationConstants.drawerIconColor),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
            /*  Container(
                child: Text('Namaz App',
                style: TextStyle(
                  color: Colors.black
                ),),

              ),*/
              Container(
                alignment: Alignment.centerRight,
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage('https://googleflutter.com/sample_image.jpg'),
                    fit: BoxFit.fill
                  )
                ),
              )
            ],
          )

        ),
        body: SafeArea(child: Column(

          children: <Widget>[
            SizedBox(
              height: 10,
             width: double.infinity,
             child: DecoratedBox(
               decoration: BoxDecoration(
                 color: Colors.white,
               ),
             )
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              shadowColor: ApplicationConstants.mainShadowColor,
              margin: EdgeInsets.only(left:15.0,right: 15.0),
              elevation: 5,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)
              ),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        ApplicationConstants.gradientStart,
                        ApplicationConstants.gradientMid,
                        ApplicationConstants.gradientEnd,
                      ]
                  ),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment:  CrossAxisAlignment.stretch,
                  children: <Widget>[
                   /* Text(
                      'Namaz Time',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Colors.white,
                        fontFamily: 'Avenir'
                      ),
                    ),*/
                    Text(
                      '12 : 30',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 80.0,
                          color: Colors.white,
                          fontFamily: 'Avenir'

                      ),
                    ),
                    Text(
                      'Islamabad',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          color: Colors.white,
                          fontFamily: 'Avenir'

                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: Table(

                        border: TableBorder.all(color: Colors.white),
                        columnWidths: const <int, TableColumnWidth>{
                          0: FlexColumnWidth(),
                          1: FlexColumnWidth(),
                          3: FlexColumnWidth(),
                          4: FlexColumnWidth(),
                          5: FlexColumnWidth(),
                      },
                        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                        children:<TableRow> [
                          TableRow(
                            children: <Widget>[

                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  'Fajar',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  'Zohr',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  'Asar',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  'Maghrib',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  'Isha',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),

                            ],
                          ),
                          TableRow(
                            children: <Widget>[

                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  '4:30 AM',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  '1:30 PM',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  '5:30 PM',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  '7:07 PM',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(5),
                                alignment: Alignment.center,
                                child: Text(
                                  '8:45 PM',
                                  style: TextStyle(
                                    color: Colors.white,
                                      fontFamily: 'Avenir'

                                  ),
                                ),
                              ),

                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,

            ),
            Expanded(
              child: ListView(

                padding: EdgeInsets.only(left: 15.0,right: 15.0),
                children: <Widget>[
                    Container(

                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(top: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                         Container(
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.circular(10),
                             color: ApplicationConstants.gradientStart
                           ),
                           child: IconButton(icon: Icon(FontAwesomeIcons.prayingHands,color: Colors.white,),
                           onPressed: (){},),

                         ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Namaz',
                              style: TextStyle(
                                  color: ApplicationConstants.fontColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Avenir'
                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.arrowBackground
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.angleRight),
                              iconSize: 18,
                              color: ApplicationConstants.arrowColor,
                              padding: EdgeInsets.all(0),
                              onPressed: (){},),
                          ),

                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(top: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.gradientStart
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.envelope,color: Colors.white,),
                              onPressed: (){},),

                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Roza',
                              style: TextStyle(
                                  color: ApplicationConstants.fontColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Avenir'

                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.arrowBackground
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.angleRight),
                              iconSize: 18,
                              color: ApplicationConstants.arrowColor,
                              padding: EdgeInsets.all(0),
                              onPressed: (){

                              },),
                          ),

                        ],
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        Navigator.of(context).push(
                        MaterialPageRoute(builder: (_)=> QuranReading())
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        margin: EdgeInsets.only(top: 5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[

                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: ApplicationConstants.gradientStart
                              ),
                              child: IconButton(icon: Icon(FontAwesomeIcons.quran,color: Colors.white,),
                                onPressed: (){},),

                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text('Quran',
                                style: TextStyle(
                                    color: ApplicationConstants.fontColor,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Avenir'

                                ),
                              ),
                            ),
                            Spacer(),
                            Container(
                              alignment: Alignment.center,
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: ApplicationConstants.arrowBackground
                              ),
                              child: IconButton(icon: Icon(FontAwesomeIcons.angleRight),
                                iconSize: 18,
                                color: ApplicationConstants.arrowColor,
                                padding: EdgeInsets.all(0),
                                onPressed: (){},),
                            ),

                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(top: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.gradientStart
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.flag,
                              color: Colors.white,
                            ),
                              onPressed: (){},),

                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Hajj',
                              style: TextStyle(
                                  color: ApplicationConstants.fontColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Avenir'

                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.arrowBackground
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.angleRight),
                              iconSize: 18,
                              color: ApplicationConstants.arrowColor,
                              padding: EdgeInsets.all(0),
                              onPressed: (){},),
                          ),

                        ],
                      ),
                    ),
                   Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(top: 5),
                     decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                         color: Colors.white
                     ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.gradientStart
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.book,color: Colors.white,),
                              onPressed: (){},),

                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Hadith',
                              style: TextStyle(
                                  color: ApplicationConstants.fontColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Avenir'

                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            alignment: Alignment.center,
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.arrowBackground
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.angleRight),
                              iconSize: 18,
                              color: ApplicationConstants.arrowColor,
                              padding: EdgeInsets.all(0),
                              onPressed: (){},
                            ),
                          ),

                        ],
                      ),
                    ),
                   Container(
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(top: 5),
                     decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(10),
                         color: Colors.white
                     ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.gradientStart
                            ),
                            child: IconButton(icon: Icon(FontAwesomeIcons.moneyCheck,color: Colors.white,),
                              onPressed: (){},),

                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Zakat',
                              style: TextStyle(
                                  color: ApplicationConstants.fontColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Avenir'

                              ),
                            ),
                          ),
                         Spacer(),
                         Container(
                              alignment: Alignment.center,
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ApplicationConstants.arrowBackground
                              ),
                              child: IconButton(icon: Icon(FontAwesomeIcons.angleRight),
                                color: ApplicationConstants.arrowColor,
                                iconSize: 18,
                                padding: EdgeInsets.all(0),
                                onPressed: (){},),
                            ),
                        ],
                      ),
                    ),


                ],
              ),
            )
          ],
        )),
      );


  }

}

