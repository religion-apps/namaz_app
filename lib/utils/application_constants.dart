import 'package:flutter/cupertino.dart';

class ApplicationConstants{
  static const appColor = Color(0xffffffff);
  static const gradientEnd = Color(0xff28cfd8);
  // static const gradientStart = Color(0xff43c4e1);
  static const gradientStart = Color(0xff339b84);

  static const gradientMid = Color(0xff2bccde);
  static const backgroundMain = Color(0xfff7fcfd);
  static const arrowBackground = Color(0xffe9f5fc);
  static const arrowColor = Color(0xff534963);
  static const mainShadowColor = Color(0xffeef3f4);
  static const fontColor = Color(0xff342c3e);
  static const drawerIconColor = Color(0xff7b99a9);
  static const settingsAppbar = Color(0xff1a1a1a);
  static const grayTextColor = Color(0xff9ab0bd);
}